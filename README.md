# Adventofcode2018

Ruby solutions for [Advent of Code 2018](https://adventofcode.com/2018).

You can find the classes used to find the solution under `lib`

These classes are used in `main.rb`, which *includes the answers as comments*.
