# frozen_string_literal: true

module Adventofcode2018
  class BoxId
    def initialize(id)
      @id = id
    end

    def contains_any_letter_exactly_twice?
      contains_any_letter_exactly_n_times?(2)
    end

    def contains_any_letter_exactly_thrice?
      contains_any_letter_exactly_n_times?(3)
    end

    def to_s
      id
    end

    def differs_by_one_positional_character?(other_id)
      mismatch_count = 0
      other_id_as_string = other_id.to_s

      id.each_char.each_with_index do |char, index|
        if char != other_id_as_string[index]
          mismatch_count += 1
          return false if mismatch_count > 1
        end
      end

      mismatch_count == 1
    end

    private

    attr_reader :id

    def contains_any_letter_exactly_n_times?(n)
      letter_frequencies.any? { |_letter, count| count == n }
    end

    def letter_frequencies
      return @letter_frequencies if defined?(@letter_frequencies)

      @letter_frequencies = Hash.new { |hash, key| hash[key] = 0 }

      id.each_char.each_with_object(@letter_frequencies) do |char, counter|
        counter[char] += 1
      end
    end
  end
end
