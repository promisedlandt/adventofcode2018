# frozen_string_literal: true

module Adventofcode2018
  class Polymer
    attr_reader :units

    class << self
      def from_file(file_path)
        from_string(File.readlines(file_path, chomp: true)
          .join(""))
      end

      def from_string(string)
        new(string.each_char.to_a)
      end

      def load
        from_file("input/polymer.txt")
      end
    end

    def initialize(units)
      @units = units
    end

    def react
      new_units = []
      unit_enumerator = units.each

      0.upto(unit_enumerator.size - 1).each do |i|
        current_unit = unit_enumerator.next
        next_unit = unit_enumerator.peek

        next unless units_react?(current_unit, next_unit)

        # Ignore current unit and next unit, return all others
        new_units = unit_enumerator.to_a[0, i] + unit_enumerator.to_a[i + 2..-1]
        break
      rescue StopIteration
        # No units have reacted, and we have reached the end
        return false
      end

      @units = new_units

      !@units.empty?
    end

    def end_result
      while react
      end

      units
    end

    def fully_reacted_length
      end_result.count
    end

    def shortest_with_removed_unit
      ("a".."z").each_with_object({}) do |letter, result|
        result[letter] = Polymer.new(units_except(letter)).fully_reacted_length
      end.min_by { |_letter, length| length }
    end

    private

    def units_except(letter)
      units.reject { |unit| unit.downcase == letter }
    end

    def units_react?(u1, u2)
      u1 != u2 && u1.downcase == u2.downcase
    end
  end
end
