# frozen_string_literal: true

module Adventofcode2018
  class BoxIdList
    class << self
      def from_file(file_path)
        new(File.readlines(file_path))
      end

      def load
        from_file("input/box_id_list.txt")
      end
    end

    def initialize(box_ids)
      @box_ids = box_ids.collect { |id| BoxId.new(id) }
    end

    # Count of ids containing a letter exactly twice,
    # multiplied by count of ids containing a letter exactly thrice.
    # An ID can count for both of these cases.
    def checksum
      @box_ids.select(&:contains_any_letter_exactly_twice?).count *
        @box_ids.select(&:contains_any_letter_exactly_thrice?).count
    end

    def off_by_one_character_matches
      candidates = box_ids

      while candidates.count > 0
        id = candidates.shift
        match = candidates.detect { |candidate| id.differs_by_one_positional_character?(candidate) }

        return [id.to_s, match.to_s] if match
      end

      []
    end

    def correct_id
      match_one, match_two = off_by_one_character_matches

      mismatch_position = 0.upto(match_one.length - 1).detect do |index|
        match_one[index] != match_two[index]
      end

      match_one[0..mismatch_position - 1] + match_one[mismatch_position + 1..-1]
    end

    private

    attr_reader :box_ids
  end
end
