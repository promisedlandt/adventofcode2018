# frozen_string_literal: true

module Adventofcode2018
  class LoopingIterator
    def initialize(*values)
      @values = values.flatten.to_enum
    end

    def enumerator
      Enumerator.new do |yielder|
        loop do
          yielder << @values.next
        rescue StopIteration
          @values.rewind
        end
      end
    end
  end
end
