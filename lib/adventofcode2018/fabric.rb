# frozen_string_literal: true

module Adventofcode2018
  class Fabric
    class << self
      def load_with_claims
        from_file("input/fabric_claims.txt")
      end

      def from_file(file_path)
        claims = File.readlines(file_path, chomp: true).collect do |claim_string|
          FabricClaim.from_string(claim_string)
        end

        new(claims: claims)
      end
    end

    def initialize(claims:)
      @claims = claims
    end

    def area_with_multiple_claims
      area_with_claims.select do |_coordinates, claim_ids|
        claim_ids.count > 1
      end.count
    end

    def non_overlapping_claims
      ids_claiming_single_points.select do |id|
        claims_with_multiple_ids.none? do |_coordinates, claim_ids|
          claim_ids.include?(id)
        end
      end
    end

    private

    attr_reader :claims

    # Build a coordinate system. For every point, save the claims on that point
    def area_with_claims
      return @claimed_areas if defined?(@claimed_areas)

      @claimed_areas = Hash.new { |hash, key| hash[key] = [] }

      @claims.each_with_object(@claimed_areas) do |claim, claimed_areas|
        claim.width_wise do |x|
          claim.height_wise do |y|
            claimed_areas[{ x: x, y: y }] << claim.id
          end
        end
      end
    end

    def ids_claiming_single_points
      @ids_claiming_single_points ||= area_with_claims.select do |_coordinates, claim_ids|
        claim_ids.count == 1
      end.collect { |_coordinates, claim_ids| claim_ids.first }.uniq
    end

    def claims_with_multiple_ids
      @claims_with_multiple_ids ||= area_with_claims.select do |_coordinates, claim_ids|
        claim_ids.count > 1
      end
    end
  end
end
