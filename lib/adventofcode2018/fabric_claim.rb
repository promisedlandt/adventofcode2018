# frozen_string_literal: true

module Adventofcode2018
  class FabricClaim
    # #123 @ 3,2: 5x4
    #  ^-- id
    #        ^-- margin_left
    #          ^-- margin_top
    #             ^-- width
    #               ^-- height
    FROM_STRING_REGEXP = /\A#(?<id>\d+) @ (?<margin_left>\d+),(?<margin_top>\d+): (?<width>\d+)x(?<height>\d+)\z/

    attr_reader :id, :margin_left, :margin_top, :width, :height

    class << self
      def from_string(string)
        named_captures = FROM_STRING_REGEXP.match(string)
                           .named_captures
                           .transform_values(&:to_i)
                           .transform_keys(&:to_sym)

        new(**named_captures)
      end
    end

    def initialize(id:, margin_left:, margin_top:, width:, height:)
      @id = id
      @margin_left = margin_left
      @margin_top = margin_top
      @width = width
      @height = height
    end

    def width_wise
      (margin_left + 1..margin_left + width).each do |x|
        yield(x)
      end
    end

    def height_wise
      (margin_top + 1..margin_top + height).each do |y|
        yield(y)
      end
    end
  end
end
