# frozen_string_literal: true

module Adventofcode2018
  class FrequencyChangeList
    class << self
      def from_file(file_path)
        new(File.readlines(file_path).collect(&:to_i))
      end

      def load
        from_file("input/frequency_change_list.txt")
      end
    end

    def initialize(change_list)
      @change_list = change_list
    end

    # The total of all changes from the change_list
    def change_by
      change_list.sum
    end

    # A change list calibrates to the first frequency it reaches twice
    def calibrates_to
      previous_results = []

      calibration = looping_change_list.inject do |sum, n|
        running_total = sum + n

        return running_total if previous_results.include?(running_total)

        previous_results << running_total

        running_total
      end

      calibration
    end

    private

    attr_reader :change_list

    def looping_change_list
      @looping_change_list ||= LoopingIterator.new(change_list).enumerator
    end
  end
end
