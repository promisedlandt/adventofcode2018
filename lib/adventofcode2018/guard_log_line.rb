# frozen_string_literal: true

module Adventofcode2018
  class GuardLogLine
    SHIFT_BEGIN_REGEXP = %r{Guard #(?<guard_id>\d+) begins shift}

    def initialize(log_line)
      @log_line = log_line
    end

    def guard_id
      return @guard_id if defined?(@guard_id)

      match_data = SHIFT_BEGIN_REGEXP.match(log_event)
      @guard_id = match_data["guard_id"] if match_data

      @guard_id
    end

    def shift_begin_line?
      !!guard_id
    end

    def falling_asleep_line?
      log_event == "falls asleep"
    end

    def wakeup_line?
      log_event == "wakes up"
    end

    def date
      if shift_time?
        timeshifted_date
      else
        parsed_date
      end
    end

    def minute
      if shift_time?
        0
      else
        # Integer() conversion does not work on 0x
        log_line[15, 2].to_i # rubocop:disable Lint/NumberConversion
      end
    end

    def timestamp
      DateTime.parse(log_line[1, 16])
    end

    private

    attr_reader :log_line

    # Since only the midnight hour is relevant, treat 23:xx as 00:00
    def shift_time?
      hour != "00"
    end

    def hour
      log_line[12, 2]
    end

    def date_string
      log_line[1, 10]
    end

    def parsed_date
      @parsed_date ||= Date.strptime(date_string, "%Y-%m-%d")
    end

    def timeshifted_date
      @timeshifted_date ||= parsed_date.next
    end

    def log_event
      log_line[19..-1]
    end
  end
end
