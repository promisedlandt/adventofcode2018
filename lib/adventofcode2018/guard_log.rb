# frozen_string_literal: true

module Adventofcode2018
  class GuardLog
    class << self
      def from_file(file_path)
        from_log_lines(File.readlines(file_path, chomp: true))
      end

      def from_log_lines(log_lines)
        guard_log_lines = log_lines.collect do |log_line|
          GuardLogLine.new(log_line)
        end

        new(guard_log_lines)
      end

      def load
        from_file("input/guard_log.txt")
      end
    end

    def initialize(log_lines)
      @log_lines = log_lines.sort_by(&:timestamp)
    end

    def most_asleep_guard
      guards.max_by(&:total_minutes_asleep)
    end

    def guard_most_asleep_on_same_minute
      guards.max_by do |guard|
        potential_count = guard.minute_asleep_at_most_with_count

        potential_count ? potential_count[1] : 0
      end
    end

    private

    attr_reader :log_lines

    def shift_begin_line_indices
      @shift_begin_line_indices ||= log_lines.each_index.select do |i|
        log_lines[i].shift_begin_line?
      end
    end

    def guards
      segmented_by_guard = Hash.new { |hash, key| hash[key] = [] }

      shift_begin_line_indices.each_cons(2) do |i, peek|
        guard_id = log_lines[i].guard_id
        segmented_by_guard[guard_id] += log_lines[i, peek - i]
      end

      # Manually add last segment :(
      last_index = shift_begin_line_indices.last
      last_guard_id = log_lines[last_index].guard_id
      segmented_by_guard[last_guard_id] += log_lines[last_index..-1]

      segmented_by_guard.collect do |guard_id, lines_for_guard|
        Guard.new(id: guard_id, log_lines: lines_for_guard)
      end
    end
  end
end
