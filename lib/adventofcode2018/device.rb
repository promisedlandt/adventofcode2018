# frozen_string_literal: true

module Adventofcode2018
  class Device
    attr_reader :current_frequency

    def initialize(starting_frequency: 0)
      @current_frequency = starting_frequency
    end

    def apply_frequency_change_list(frequency_change_list)
      @current_frequency += frequency_change_list.change_by
    end

    def calibrate_with_frequency_change_list(frequency_change_list)
      @current_frequency = frequency_change_list.calibrates_to
    end
  end
end
