# frozen_string_literal: true

module Adventofcode2018
  class Guard
    attr_reader :id

    def initialize(id:, log_lines:)
      @id = id
      @log_lines = log_lines
    end

    def total_minutes_asleep
      minutes_asleep_by_date.inject(0) do |sum, (_date, minutes_asleep)|
        sum + minutes_asleep.count
      end
    end

    def minute_asleep_at_most_with_count
      minutes_asleep_at_count.max_by do |_minute, count|
        count
      end
    end

    def log_lines_by_date
      Hash[log_lines.chunk(&:date).collect do |date, lines_for_date|
        [date, lines_for_date]
      end]
    end

    def minutes_asleep_by_date
      @minutes_asleep_by_date ||= Hash[log_lines_by_date.collect do |date, lines_for_date|
        minutes_asleep = []

        falling_asleep_lines = lines_for_date.select(&:falling_asleep_line?)
        wakeup_lines = lines_for_date.select(&:wakeup_line?)

        falling_asleep_lines.each_with_index do |falling_asleep_line, index|
          wakeup_line = wakeup_lines[index]
          last_minute_asleep = wakeup_line ? wakeup_line.minute - 1 : 59
          minutes_asleep += (falling_asleep_line.minute..last_minute_asleep).to_a
        end

        [date, minutes_asleep]
      end
      ]
    end

    private

    attr_reader :log_lines

    def minutes_asleep_at_count
      minutes_asleep_at_count = Hash.new { |hash, key| hash[key] = 0 }

      minutes_asleep_by_date.each_with_object(minutes_asleep_at_count) do |(_date, minutes_asleep_by_date), count|
        minutes_asleep_by_date.each do |minute|
          count[minute] += 1
        end
      end
    end
  end
end
