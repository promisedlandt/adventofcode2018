# frozen_string_literal: true

require_relative "adventofcode2018/version"

require_relative "adventofcode2018/looping_iterator"
require_relative "adventofcode2018/device"
require_relative "adventofcode2018/frequency_change_list"

require_relative "adventofcode2018/box_id"
require_relative "adventofcode2018/box_id_list"

require_relative "adventofcode2018/fabric_claim"
require_relative "adventofcode2018/fabric"

require "date"
require_relative "adventofcode2018/guard"
require_relative "adventofcode2018/guard_log_line"
require_relative "adventofcode2018/guard_log"

require_relative "adventofcode2018/polymer"

module Adventofcode2018
end
