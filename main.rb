# frozen_string_literal: true

require "bundler/setup"
require "adventofcode2018"

puts "01: Starting with a frequency of zero, what is the resulting frequency after all of the changes in frequency have been applied?"

frequency_change_list = Adventofcode2018::FrequencyChangeList.load
device = Adventofcode2018::Device.new
device.apply_frequency_change_list(frequency_change_list)

puts "Day 1"

# 435
puts device.current_frequency

puts "02: What is the first frequency your device reaches twice?"
#device.calibrate_with_frequency_change_list(frequency_change_list)

# 245
#puts device.current_frequency

puts "-----------------------------------------------------------"
puts "Day 2"

puts "03: What is the checksum for your list of box IDs?"

box_id_list = Adventofcode2018::BoxIdList.load

# 7192
puts box_id_list.checksum

puts "04: What letters are common between the two correct box IDs?"

# mbruvapghxlzycbhmfqjonsie
puts box_id_list.correct_id

puts "-----------------------------------------------------------"
puts "Day 3"

puts "05: How many square inches of fabric are within two or more claims?"

fabric = Adventofcode2018::Fabric.load_with_claims

# 118223
#puts fabric.area_with_multiple_claims

puts "06: What is the ID of the only claim that doesn't overlap?"

# 412
#puts fabric.non_overlapping_claims

puts "-----------------------------------------------------------"
puts "Day 4"
puts "07: What is the ID of the guard you chose multiplied by the minute you chose?"

guard_log = Adventofcode2018::GuardLog.load
most_asleep_guard = guard_log.most_asleep_guard
minute_asleep_at_most = most_asleep_guard.minute_asleep_at_most_with_count.first

# 1439 * 42 = 60438
puts "#{ most_asleep_guard.id } * #{ minute_asleep_at_most } = #{ Integer(most_asleep_guard.id) * minute_asleep_at_most }"

puts "08: What is the ID of the guard you chose multiplied by the minute you chose?"

guard_most_asleep_on_same_minute = guard_log.guard_most_asleep_on_same_minute
minute = guard_most_asleep_on_same_minute.minute_asleep_at_most_with_count.first

# 1297 * 37 = 47989
puts "#{ guard_most_asleep_on_same_minute.id } * #{ minute } = #{ Integer(guard_most_asleep_on_same_minute.id) * minute }"

puts "-----------------------------------------------------------"
puts "Day 5"
puts "09: How many units remain after fully reacting the polymer you scanned?"

polymer = Adventofcode2018::Polymer.load

# 9686
#puts polymer.fully_reacted_length

puts "10 What is the length of the shortest polymer you can produce by removing all units of exactly one type and fully reacting the result?"

# 5524
#puts polymer.shortest_with_removed_unit
