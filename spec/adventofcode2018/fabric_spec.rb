# frozen_string_literal: true

RSpec.describe Adventofcode2018::Fabric do
  shared_context "fabric_with_one_claim" do
    let(:claims) { [claim] }

    let(:claim) do
      Adventofcode2018::FabricClaim.new(
        id: 1,
        margin_left: 0,
        margin_top: 0,
        width: 5,
        height: 4)
    end
  end

  shared_context "fabric_with_two_completely_overlapping_claims" do
    let(:claims) { [claim_1, claim_2] }

    let(:claim_1) do
      Adventofcode2018::FabricClaim.new(
        id: 1,
        margin_left: 0,
        margin_top: 0,
        width: 5,
        height: 4)
    end

    let(:claim_2) do
      Adventofcode2018::FabricClaim.new(
        id: 2,
        margin_left: 0,
        margin_top: 0,
        width: 5,
        height: 4)
    end
  end

  shared_context "fabric_example_from_website" do
    let(:claims) { [claim_1, claim_2, claim_3] }

    let(:claim_1) do
      Adventofcode2018::FabricClaim.new(
        id: 1,
        margin_left: 1,
        margin_top: 3,
        width: 4,
        height: 4)
    end

    let(:claim_2) do
      Adventofcode2018::FabricClaim.new(
        id: 2,
        margin_left: 3,
        margin_top: 1,
        width: 4,
        height: 4)
    end

    let(:claim_3) do
      Adventofcode2018::FabricClaim.new(
        id: 3,
        margin_left: 5,
        margin_top: 5,
        width: 2,
        height: 2)
    end
  end

  subject(:fabric) { described_class.new(claims: claims) }

  describe "#area_with_multiple_claims" do
    context "one claim" do
      include_context "fabric_with_one_claim"

      it "returns 0" do
        expect(fabric.area_with_multiple_claims).to eq 0
      end
    end

    context "two completely overlapping claims" do
      include_context "fabric_with_two_completely_overlapping_claims"

      it "returns the area of those claims" do
        expect(fabric.area_with_multiple_claims).to eq(claim_1.width * claim_2.height)
      end
    end

    context "provided example from website" do
      include_context "fabric_example_from_website"
      it "returns 4" do
        expect(fabric.area_with_multiple_claims).to eq 4
      end
    end
  end

  describe "#non_overlapping_claims" do
    context "with only one claim" do
      include_context "fabric_with_one_claim"

      it "returns the id of that claim" do
        expect(fabric.non_overlapping_claims).to eq [claim.id]
      end
    end

    context "two completely overlapping claims" do
      include_context "fabric_with_two_completely_overlapping_claims"

      it "returns empty array" do
        expect(fabric.non_overlapping_claims).to eq []
      end
    end

    context "provided example from website" do
      include_context "fabric_example_from_website"

      it "returns [3]" do
        expect(fabric.non_overlapping_claims).to eq [3]
      end
    end
  end
end
