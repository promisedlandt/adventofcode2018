# frozen_string_literal: true

RSpec.describe Adventofcode2018::BoxId do
  subject(:box_id) { described_class.new(id) }

  describe "#contains_any_letter_exactly_twice?" do
    { "abcdef" => false,
      "bababc" => true,
      "abbcde" => true,
      "abcccd" => false,
      "aabcdd" => true,
      "abcdee" => true,
      "ababab" => false,
    }.each do |test_case_id, expected_result|
      context "when id is #{ test_case_id }" do
        let(:id) { test_case_id }

        it "returns #{ expected_result }" do
          expect(box_id.contains_any_letter_exactly_twice?).to eq expected_result
        end
      end
    end
  end

  describe "#contains_any_letter_exactly_thrice?" do
    { "abcdef" => false,
      "bababc" => true,
      "abbcde" => false,
      "abcccd" => true,
      "aabcdd" => false,
      "abcdee" => false,
      "ababab" => true,
    }.each do |test_case_id, expected_result|
      context "when id is #{ test_case_id }" do
        let(:id) { test_case_id }

        it "returns #{ expected_result }" do
          expect(box_id.contains_any_letter_exactly_thrice?).to eq expected_result
        end
      end
    end
  end

  describe "#differs_by_one_positional_character?" do
    { %w(fghij fguij) => true, # 1 difference
      %w(abcde wvxyz) => false, # 5 differences
      %w(asdfg asdfg) => false, # 0 differences
      %w(fghij fghzx) => false # 2 differences
    }.each do |ids, expected_result|
      context "with #{ ids }" do
        let(:id) { ids[0] }
        let(:other_id) { described_class.new(ids[1]) }

        it "returns #{ expected_result }" do
          expect(box_id.differs_by_one_positional_character?(other_id)).to eq expected_result
        end
      end
    end
  end
end
