# frozen_string_literal: true

RSpec.describe Adventofcode2018::FrequencyChangeList do
  describe "#change_by" do
    [
      { change_list: [1, 1, 1],
        expected_frequency: 3 },
      { change_list: [1, 1, -2],
        expected_frequency: 0 },
      { change_list: [-1, -2, -3],
        expected_frequency: -6 },
    ].each do |test_case|
      context "change list #{ test_case[:change_list] }" do
        subject(:frequency_change_list) { described_class.new(test_case[:change_list]) }

        it "changes by #{ test_case[:expected_frequency] }" do
          expect(frequency_change_list.change_by).to eq test_case[:expected_frequency]
        end
      end
    end
  end

  describe "#calibrates_to" do
    { [1, -1] => 0,
      [3, 3, 4, -2, -4] => 10,
      [-6, 3, 8, 5, -6] => 5,
      [7, 7, -2, -7, -4] => 14,
    }.each do |change_list, expected_calibration|
      context "change list #{ change_list }" do
        subject(:frequency_change_list) { described_class.new(change_list) }

        it "calibrates to #{ expected_calibration }" do
          expect(frequency_change_list.calibrates_to).to eq expected_calibration
        end
      end
    end
  end
end
