# frozen_string_literal: true

RSpec.describe Adventofcode2018::Polymer do
  subject(:polymer) { described_class.from_string(units) }

  {
    "aA" => [[]],
    "abBA" => [%w(a A), []],
    "abAB" => [%w(a b A B)],
    "aabAAB" => [%w(a a b A A B)],
    "dabAcCaCBAcCcaDA" => [%w(d a b A a C B A c C c a D A),
                           %w(d a b C B A c C c a D A),
                           %w(d a b C B A c a D A)],
    "cabBAC" => [%w(c a A C),
                 %w(c C),
                 []],
  }.each do |units, expected_output|
    context "given #{ units }" do
      let(:units) { units }

      describe "#react" do
        it "reacts" do
          expected_output.each do |iteration_output|
            polymer.react
            expect(polymer.units).to eq iteration_output
          end
        end
      end

      describe "#end_result" do
        it "returns expected end result" do
          expect(polymer.end_result).to eq expected_output.last
        end
      end
    end
  end

  describe "#shortest_with_removed_unit" do
    context "example from website" do
      let(:units) { "dabAcCaCBAcCcaDA" }

      it "returns ['c', 4]" do
        expect(polymer.shortest_with_removed_unit).to eq ["c", 4]
      end
    end
  end
end
