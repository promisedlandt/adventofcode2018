# frozen_string_literal: true

RSpec.describe Adventofcode2018::LoopingIterator do
  subject(:iterator) { described_class.new(values).enumerator }

  context "when initialized with one element" do
    let(:values) { 1 }

    it "returns that element multiple times" do
      expect(iterator.take(3)).to eq [1, 1, 1]
    end
  end

  context "when initialized with two elements" do
    let(:values) { [1, 2] }

    it "cycles through the elements" do
      expect(iterator.take(5)).to eq [1, 2, 1, 2, 1]
    end
  end
end
