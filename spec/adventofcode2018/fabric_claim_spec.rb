# frozen_string_literal: true

RSpec.describe Adventofcode2018::FabricClaim do
  describe ".from_string" do
    context "given #123 @ 3,2: 5x4" do
      subject(:claim) { described_class.from_string(claim_string) }

      let(:claim_string) { "#123 @ 3,2: 5x4" }

      it "initializes the correct FabricClaim" do
        expect(claim.id).to eq 123
        expect(claim.margin_left).to eq 3
        expect(claim.margin_top).to eq 2
        expect(claim.width).to eq 5
        expect(claim.height).to eq 4
      end
    end
  end
end
