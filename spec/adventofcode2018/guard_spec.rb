# frozen_string_literal: true

RSpec.describe Adventofcode2018::Guard do
  context "example guard #99 from website" do
    subject(:guard) { described_class.new(id: 99, log_lines: log_lines) }

    let(:log_lines) do
      [
        "[1518-11-01 23:58] Guard #99 begins shift",
        "[1518-11-02 00:40] falls asleep",
        "[1518-11-02 00:50] wakes up",
        "[1518-11-04 00:02] Guard #99 begins shift",
        "[1518-11-04 00:36] falls asleep",
        "[1518-11-04 00:46] wakes up",
        "[1518-11-05 00:03] Guard #99 begins shift",
        "[1518-11-05 00:45] falls asleep",
        "[1518-11-05 00:55] wakes up",
      ].collect { |line| Adventofcode2018::GuardLogLine.new(line) }
    end

    let(:first_date) { Date.new(1518, 11, 2) }
    let(:second_date) { Date.new(1518, 11, 4) }
    let(:third_date) { Date.new(1518, 11, 5) }

    describe "#log_lines_by_date" do
      let(:log_lines_by_date) { guard.log_lines_by_date }

      it "returns the log lines by day" do
        expect(log_lines_by_date[first_date].length).to eq 3
        expect(log_lines_by_date[second_date].length).to eq 3
        expect(log_lines_by_date[third_date].length).to eq 3
      end
    end

    describe "#minutes_asleep_by_date" do
      let(:minutes_asleep_by_date) { guard.minutes_asleep_by_date }

      it "returns an array of minutes at which the guard was asleep" do
        expect(minutes_asleep_by_date[first_date]).to eq((40..49).to_a)
        expect(minutes_asleep_by_date[second_date]).to eq((36..45).to_a)
        expect(minutes_asleep_by_date[third_date]).to eq((45..54).to_a)
      end
    end

    describe "#total_minutes_asleep" do
      it "returns 30" do
        expect(guard.total_minutes_asleep).to eq 30
      end
    end

    describe "#minute_asleep_at_most" do
      it "returns 45" do
        expect(guard.minute_asleep_at_most_with_count.first).to eq 45
      end
    end
  end

  context "example guard #10 from website" do
    subject(:guard) { described_class.new(id: 10, log_lines: log_lines) }

    let(:log_lines) do
      [
        "[1518-11-01 00:00] Guard #10 begins shift",
        "[1518-11-01 00:05] falls asleep",
        "[1518-11-01 00:25] wakes up",
        "[1518-11-01 00:30] falls asleep",
        "[1518-11-01 00:55] wakes up",
        "[1518-11-03 00:05] Guard #10 begins shift",
        "[1518-11-03 00:24] falls asleep",
        "[1518-11-03 00:29] wakes up",
      ].collect { |line| Adventofcode2018::GuardLogLine.new(line) }
    end

    let(:first_date) { Date.new(1518, 11, 1) }
    let(:second_date) { Date.new(1518, 11, 3) }

    describe "#log_lines_by_date" do
      let(:log_lines_by_date) { guard.log_lines_by_date }

      it "returns the log lines by day" do
        expect(log_lines_by_date[first_date].length).to eq 5
        expect(log_lines_by_date[second_date].length).to eq 3
      end
    end

    describe "#minutes_asleep_by_date" do
      let(:minutes_asleep_by_date) { guard.minutes_asleep_by_date }

      it "returns an array of minutes at which the guard was asleep" do
        expect(minutes_asleep_by_date[first_date]).to eq((5..24).to_a + (30..54).to_a)
        expect(minutes_asleep_by_date[second_date]).to eq((24..28).to_a)
      end
    end

    describe "#total_minutes_asleep" do
      it "returns 50" do
        expect(guard.total_minutes_asleep).to eq 50
      end
    end

    describe "#minute_asleep_at_most" do
      it "returns 24" do
        expect(guard.minute_asleep_at_most_with_count.first).to eq 24
      end
    end
  end
end
