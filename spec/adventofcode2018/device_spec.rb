# frozen_string_literal: true

RSpec.describe Adventofcode2018::Device do
  describe "#apply_frequency_change_list" do
    [
      { starting_frequency: 0,
        change_by: 1,
        expected_frequency: 1 },
      { starting_frequency: 1,
        change_by: -2,
        expected_frequency: -1 },
      { starting_frequency: -1,
        change_by: 3,
        expected_frequency: 2 },
      { starting_frequency: 2,
        change_by: 1,
        expected_frequency: 3 },
    ].each do |test_case|
      context "starting with #{ test_case[:starting_frequency] } and changing by #{ test_case[:change_by] }" do
        subject(:device) { described_class.new(starting_frequency: test_case[:starting_frequency]) }

        let(:frequency_change_list) { double(Adventofcode2018::FrequencyChangeList, change_by: test_case[:change_by]) }

        it "results in #{ test_case[:expected_frequency] }" do
          device.apply_frequency_change_list(frequency_change_list)

          expect(device.current_frequency).to eq test_case[:expected_frequency]
        end
      end
    end
  end
end
