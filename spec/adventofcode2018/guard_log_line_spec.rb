# frozen_string_literal: true

RSpec.describe Adventofcode2018::GuardLogLine do
  subject(:log_line) { described_class.new(input) }

  SHIFT_BEGIN_LINE = "[1518-11-01 00:00] Guard #10 begins shift"
  FALLING_ASLEEP_LINE = "[1518-11-01 00:05] falls asleep"
  WAKEUP_LINE = "[1518-11-01 00:25] wakes up"

  describe "#guard_id" do
    let(:input) { SHIFT_BEGIN_LINE }

    it "returns '10'" do
      expect(log_line.guard_id).to eq "10"
    end
  end

  describe "#date" do
    context "when timestamp is in midnight hour" do
      let(:input) { "[1518-06-01 00:41]" }

      it "returns date from log line" do
        expect(log_line.date).to eq Date.new(1518, 6, 1)
      end
    end

    context "when timestamp starts at 23:xx" do
      let(:input) { "[1518-06-01 23:41]" }

      it "returns next date" do
        expect(log_line.date).to eq Date.new(1518, 6, 2)
      end
    end
  end

  describe "#date" do
    context "when timestamp is in midnight hour" do
      let(:input) { "[1518-06-01 00:41]" }

      it "returns time from log line" do
        expect(log_line.minute).to eq 41
      end
    end

    context "when timestamp starts at 23:xx" do
      let(:input) { "[1518-06-01 23:41]" }

      it "returns 0" do
        expect(log_line.minute).to eq 0
      end
    end
  end

  describe "#shift_begin_line?" do
    { SHIFT_BEGIN_LINE => true,
      FALLING_ASLEEP_LINE => false,
      WAKEUP_LINE => false,
    }.each do |text, expected_result|
      context "given #{ text }" do
        let(:input) { text }

        it "returns #{ expected_result }" do
          expect(log_line.shift_begin_line?).to eq expected_result
        end
      end
    end
  end

  describe "#wakeup_line?" do
    { SHIFT_BEGIN_LINE => false,
      FALLING_ASLEEP_LINE => false,
      WAKEUP_LINE => true,
    }.each do |text, expected_result|
      context "given #{ text }" do
        let(:input) { text }

        it "returns #{ expected_result }" do
          expect(log_line.wakeup_line?).to eq expected_result
        end
      end
    end
  end

  describe "#falling_asleep_line?" do
    { SHIFT_BEGIN_LINE => false,
      FALLING_ASLEEP_LINE => true,
      WAKEUP_LINE => false,
    }.each do |text, expected_result|
      context "given #{ text }" do
        let(:input) { text }

        it "returns #{ expected_result }" do
          expect(log_line.falling_asleep_line?).to eq expected_result
        end
      end
    end
  end
end
