# frozen_string_literal: true

RSpec.describe Adventofcode2018::BoxIdList do
  subject(:list) { described_class.new(box_ids) }

  describe "#checksum" do
    context "provided example" do
      let(:box_ids) do
        %w[ abcdef
            bababc
            abbcde
            abcccd
            aabcdd
            abcdee
            ababab
        ]
      end

      it "returns 12" do
        expect(list.checksum).to eq 12
      end
    end
  end

  describe "#off_by_one_character_matches" do
    context "provided example" do
      let(:box_ids) do
        %w[ abcde
            fghij
            klmno
            pqrst
            fguij
            axcye
            wvxyz
        ]
      end

      it "returns fghij and fguij" do
        expect(list.off_by_one_character_matches).to eq %w(fghij fguij)
      end
    end
  end

  describe "#correct_id" do
    context "provided example" do
      let(:box_ids) do
        %w[ abcde
            fghij
            klmno
            pqrst
            fguij
            axcye
            wvxyz
        ]
      end

      it "returns fgij" do
        expect(list.correct_id).to eq "fgij"
      end
    end
  end
end
